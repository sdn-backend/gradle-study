## Gradle

## 목차
1. [멀티모듈이란](#멀티모듈이란)
2. [build.grade](#build)
3. [setting.grade](#setting)
4. [Gradle 개념](#Gradle-개념)
5. [Groovy](#Groovy)

## 멀티모듈이란
- 모듈을 패키지의 한 단계 위의 집합체이며, 관련된 패키지와 리소스들의 재사용할 수 있는 그룹(오라클 자바문서) 

### 멀티모듈 사용 이유
1. 코드의 중복을 줄일 수 있다.
2. 각 모듈의 기능을 파악하기 쉬워진다.
3. 빌드를 쉽게 진행 가능

### 멀티모듈 간단 설정 방법
1. 기본 프로젝트 생성(gradle 프로젝트로 생성)
2. `File > Project Structure` 탭에서 모듈 추가
3. `Root Project(setting.gradle)` 

```java
rootProject.name = 'multi-module'

include 'library'
include 'application'
```

- `library` 모듈 내 `bulid.gradle`
    - 기본적으로 gradle build시 실행 가능한 `jar` 파일을 만드는데, library는 자체적으로 실행하지 않으므로 false로 설정해준다.

```java
plugins {
id 'org.springframework.boot' version '2.5.2'
id 'io.spring.dependency-management' version '1.0.11.RELEASE'
id 'java'
}

group = 'com.example'
version = '0.0.1-SNAPSHOT'
sourceCompatibility = '1.8'

repositories {
mavenCentral()
}

// 설정 부분
bootJar {
enabled = false
}

jar {
enabled = true
}
//

dependencies {
implementation 'org.springframework.boot:spring-boot-starter'
testImplementation 'org.springframework.boot:spring-boot-starter-test'
}
```

- `application` 모듈 내 `bulid.gradle`
    - `project(:projectPath)` 방식으로 정의한 모듈의 의존성을 추가하여 사용 가능

```java
plugins {
	id 'org.springframework.boot' version '2.5.2'
	id 'io.spring.dependency-management' version '1.0.11.RELEASE'
	id 'java'
}

group = 'com.example'
version = '0.0.1-SNAPSHOT'
sourceCompatibility = '1.8'

repositories {
	mavenCentral()
}

dependencies {
	implementation 'org.springframework.boot:spring-boot-starter-actuator'
	implementation 'org.springframework.boot:spring-boot-starter-web'
  
  // 설정 부분
	implementation project(':library')

	testImplementation 'org.springframework.boot:spring-boot-starter-test'
}
```

## build
- `buildscript`
    - gradle 빌드 스크립트 자체를 위한 __의존성이나 변수 Task, Plugin__ 을 지정할 수 있다.
- `allprojects` 
    - 모든 프로젝트의 공통된 부분
    - root project 마저 같이 설정 (일반적으로 루트 프로젝트는 폴더명처럼 하고 서브 프로젝트에 주로 설정)
- `subprojects` 
    - 각각의 하위 프로젝트의 공통된 부분
  

## setting
- 전체 프로젝트의 구조 빌드

- 일반적인 구조
```java
rootProject.name="project-name"
include "project-name"
```

- 여러 모듈 프로젝트를 포함하는 경우
```java
rootProject.name="project-name"
include ":sub-project1"
include ":sub-project2"
```
- group으로 관리하고 싶다면 자동 빌드하는 스크립트 작성 가능

## Gradle 개념
> [참고자료](https://willbesoon.tistory.com/93)
### 특징
- Ant처럼 매우 유연한 범용 빌드 도구
- Maven과 같은 구조화 된 build 프레임워크(구조의 전환 가능)
- Maven, Ivy 등의 기존 저장소 인프라 또는 pom.xml 파일과 ivy.xml 파일에 대한 migration의 편이성 제공
- `멀티 프로젝트` 빌드 지원 ✅
- 의존성 관리의 다양한 방법 제공
- Build script를 xml이 아닌 Groov 기반의 DSL을 사용 
- 기존 Build를 구성하기 위한 풍부한 도메인 모델 제공
- Gradle 설치 없이 Gadle Wrapper를 이용하여 빌드 지원

### 장점
- Ant, Maven과 같은 기존 빌드툴은 xml형식을 이용하여 정적인 설정정보를 구성했지만, Gradle은 Groovy라는 언어를 이용하여
코드로서 설정정보를 구성하기 때문에 구조적인 장점이 있다.
- xml의 구조적인 틀을 벗어나 코딩에 의한 간단한 정의가 가능 
- 프로젝트를 설정주입방식으로 정의하기 때문에 maven의 상속 구조보다 재사용에 용이하다.

### 기본 구조
- 모든 Gradle script는 하나 이상의 project로 구성되며, 모든 프로젝트는 하나이상의 task로 구성
  - `Project` : 소스를 jar로 모으거나, 자바 프로젝트를 컴파일하거나, 테스트를 실행하고, 어플리케이션을 배포하는 등의 업무로 구성
  - `Task` : 작업의 최소 단위 / Task간의 의존관계 설정과 함께 흐름에 따른 구성이 가능하며, 동적인 테스크의 생성도 가능
- Gradle은 자바6버전 이상의 VM환경에서 사용이 가능하며, 설치를 하거나 gradle wrapper를 이용하여 실행환경을 구성 가능

|디렉토리/파일|설명|
|----|----|
|/.gradle <br> /gradle| gradle 버전별 엔진 및 설정 파일|
|/.idea|에디터 관련 파일들| 
|/gradlew <br> /gradlew.bat|gradle 명령파일|
|/setting.gradle|빌드할 프로젝트 정보 설정<br>(트리형태로 멀티프로젝트 구성)|
|/build.gradle|프로젝트 빌드에 대한 모든 기능 정의|
|/src|자바 소스 파일|

## Groovy
- `Gradle`
  - ✅ `Groovy` 기반의 빌드 자동화 오픈 소스
  - 빌드 스크립트를 xml이 아닌 `Groovy`로 작성

- `Gradle` vs `Maven`
  - Gradle의 Groovy 언어를 이용한 스크립트 작성 vs Maven의 xml 작성
  - Gradle의 주입 방식 vs Maven 상속 방식
  - Gradle이 Maven보다 빌드속도가 더 뛰어남
### 개념
- 특정 도미엔에 특화된 언어인 DSL
- JVM위에서 돌아가며 Java에 파이썬, 루비 등의 특징을 얹었기 때문에 Java와 문법이 유사
- Gradle wrapper를 이용 gradle이 설치되어있지 않아도 사용 가능(버전 신경 x)
- 확장성 뛰어남

### 문법
#### 문자열 표현
- Groovy의 문자열 표현은 파이썬과 유사
- 작은 따옴표: 단순히 문자열 출력 
- 큰 따옴표: 문자열 출력에 사용되는데 내부에 `$변수명` 형태의 템플릿 문자열 구성 가능
- 3개로 묶기: `"""~~"""` 여러줄을 표현할 때 사용